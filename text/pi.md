## Kreiszahl π

*Code: [pi.lua](../code/pi.lua)*

Die Kreiszahl π lässt sich mit folgender Formel berechnen:

![Pi](pi.png)

Der Zähler der Brüche ist immer 4, die Nenner sind die ungeraden Zahlen und die Brüche werden abwechselnd addiert und subtrahiert. Wir können natürlich nicht unendliche viele Brüche zusammenrechnen, aber wir können versuchen einen Näherungswert zu ermitteln, indem wir die erste Million Brüche in einer Schleife durchgehen.

Den Nenner erhalten wir, indem wir die Schleifenvariable zuerst mit 2 malnehmen (wir erhalten 2, 4, 6...) und dann 1 abziehen (1, 3, 5...).

```lua
local fraction = 4 / (2 * i - 1)
```

Wenn wir die Schleifenvariable modulo 2 nehmen, erhalten wir abwechselnd 1 und 0 und können daran festmachen, ob wir den Bruch dazuzählen oder abziehen.

```lua
if i % 2 == 1 then
    pi = pi + fraction
else
    pi = pi - fraction
end
```
