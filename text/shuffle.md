## Karten mischen

*Code: [shuffle.lua](../code/shuffle.lua)*

Um ein Array zu mischen, stellen wir uns vor, dass es aus zwei Bereichen besteht: Dem ungemischten Bereich vorne und dem gemischten Bereich hinten. Zu beginnt gehört das ganze Array zum ungemischten Bereich. Wir wählen nun immer wieder ein zufälliges Element im ungemischten Bereich aus und verschieben es ans Ende des Bereichs. Der ungemischte Bereich wird dabei immer um eins kleiner.

```lua
local function shuffle(array)
    for i = #array, 2, -1 do
        local j = random() % i + 1
        array[i], array[j] = array[j], array[i]
    end
end
```

(In Lua kann man zwei Variablen mit `a, b = b, a` vertauschen. Das geht aber nicht in jeder Sprache, gegebenenfalls muss man dafür eine Hilfsvariable nehmen.)

Mit diesem Algorithmus können wir auch ein Array von Spielkarten mischen und Kartenspiele programmieren.
