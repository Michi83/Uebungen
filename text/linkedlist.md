## Verknüpfte Listen

*Code: [linkedlist.lua](../code/linkedlist.lua)*

Eine verknüpfte Liste ist eine Alternative zu einem Array, wenn man Daten in eine Reihenfolge bringen möchte. Eine verknüpfte Liste besteht aus einzelnen Zellen, die je ein Element der Liste und einen Verweis auf die nächste Zelle enthalten. Die letzte Zelle verweist auf `nil`. Bildlich kann man das so darstellen:

![Verknüpfte Liste](linkedlist.png)

Im Beispielcode enthält die Liste selbst nur einen Verweis auf ihre erste Zelle (`first`) und ihre Länge.

```lua
local list = {first = nil, length = 0}
```

Alle weiteren Zellen können wir erreichen, indem wir jeweils `cell.next` zur nächsten Zelle folgen, wie wir beispielhaft an der Funktion `get` sehen, die ein Element der Liste zurückgibt:

```lua
function list.get(index)
    local cell = list.first
    for i = 1, index - 1 do
        cell = cell.next
    end
    return cell.item
end
```

Der Beispielcode implementiert außerdem folgende Funktionen:

* `prepend` fügt am Anfang der Liste ein neues Element hinzu.
* `insertafter` fügt hinter einem bestehenden Element ein neues Element ein.
* `set` ändert ein Element.
* `delete` entfernt ein Element aus der Liste.

Einige dieser Funktionen könnte man auch als Operatorüberladung implementieren, so dass z.B. die Schreibweise `list[index]` statt `list.get(index)` möglich wird. Das wird aber im Beispielcode nicht gezeigt.

Verknüpfte Listen können auch eine ringförmige Struktur annehmen, wenn die letzte Zelle wieder zurück auf die erste verweist. Ebenfalls möglich sind doppelt verknüpfte Listen, in denen die Zellen zusätzlich zum Nachfolger auch auf den Vorgänger verweisen.
