## Bubblesort

*Code: [bubblesort.lua](../code/bubblesort.lua)*

Bubblesort ist ein Algorithmus zum Sortieren von Arrays. Wir gehen dabei das Array in einer Schleife durch und vergleichen jedes Element mit seinem Nachfolger. Wenn das Element größer ist als der Nachfolger, dann vertauschen wir sie. Es ist klar, dass das größte Element im Array jeden dieser Vergleiche "gewinnt" und durch die Vertauschungen ganz nach hinten wandert. Wenn wir dasselbe jetzt noch mal mit dem vorderen Rest des Arrays machen, dann wandert auch das zweitgrößte Element an die vorletzte Stelle usw. Wir wiederholen also die Schleife selbst in einer Schleife.

```lua
for i = #array, 2, -1 do
    for j = 1, i - 1 do
        if array[j] > array[j + 1] then
            array[j], array[j + 1] = array[j + 1], array[j]
        end
    end
end
```
