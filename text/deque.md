## Stacks und Queues

*Code: [deque.lua](../code/deque.lua)*

Ein Stack (Stapel) ist eine Datenstruktur, die nach dem LIFO-Prinzip (last in, first out) funktioniert. Wir können am Ende Elemente hinzufügen und wieder entfernen. Das letzte Element, das wir dabei hinzugefügt haben, ist das erste, das wir wieder entfernen. Bildlich kann man sich einen Stack wie einen Stapel Notizzettel vorstellen. Man kann oben einen weiteren Zettel drauflegen oder den obersten Zettel wegnehmen. (Das Hinzufügen wird oft "push" genannt, das Entfernen "pop".)

Eine Queue (Warteschlange) funktioniert dagegen nach dem FIFO-Prinzip (first in, first out). Das erste Element, das wir hinzufügen, ist auch das erste, das wir wieder entnehmen.

Eine Verallgemeinerung davon ist die Deque (sprich Deck; double-ended queue). Bei einer Deque können wir an beiden Enden Elemente hinzufügen und entfernen. Das heißt wir können eine Deque an beiden Enden als Stack und in beiden Richtungen als Queue verwenden.

In Lua können wir Arrays als Deques verwenden. Am Anfang fügen wir Elemente mit `table.insert(array, 1, item)` hinzu und entfernen sie mit `table.remove(array, 1)`. Am Ende des Arrays verwenden wir `table.insert(array, item)` und `table.remove(array)`.

Trotzdem wollen wir uns einmal überlegen, wie wir eine Deque selbst implementieren können. Eine Möglichkeit ist, dass wir dazu eine ringförmige, doppelt [verknüpfte Liste](linkedlist.md) verwenden. Die Liste wird schon zu Beginn eine "Dummy-Zelle" namens `head` enthalten. Dadurch vereinfacht sich nämlich einiges, insbesondere müssen wir den Sonderfall einer leeren Liste nicht berücksichtigen. Links und rechts von `head` werden dann Elemente hinzugefügt und entfernt.

![Deque](deque.png)

`head` wird gleich zu Beginn mit sich selbst verknüft. Dadurch haben wir von Anfang an eine Ringstruktur, auch wenn wir der Deque noch keine Elemente hinzugefügt haben.

```lua
local head = {}
head.next = head
head.prev = head
```

Um ein Element hinzuzufügen, erzeugen wir eine neue Zelle und flechten sie in die Zelle ein:

```lua
function deque.pushfront(item)
    local cell = {item = item, prev = head, next = head.next}
    cell.next.prev = cell
    cell.prev.next = cell
    length = length + 1
end
```

Um ein Element wieder zu entfernen, lösen wir die Zelle wieder aus dem Ring:

```lua
function deque.popfront()
    if length == 0 then return nil end
    local cell = head.next
    cell.next.prev = cell.prev
    cell.prev.next = cell.next
    length = length - 1
    return cell.item
end
```
