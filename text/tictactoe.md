## Tic-Tac-Toe

*Code: [tictactoe.lua](../code/tictactoe.lua)*

Viele Spiele wie [Tic-Tac-Toe](https://de.wikipedia.org/wiki/Tic-Tac-Toe) oder Schach können wir uns als [Baumstrukturen](tree.md) vorstellen: Die Ausgangsstellung ist die Wurzel und jeder gültige Zug führt zu einem Kindknoten. Die Blätter dieses Baums sind Stellungen in denen das Spiel zu Ende ist. Ein Computergegner muss in diesem Baum den besten Zug finden. Dafür kommt der sogenannte Minimax-Algorithmus zum Einsatz.

Eine Stellung repräsentieren wir als zweidimensionales 3*3-Array von Zahlen. Darin steht 1 für X, -1 für O und 0 für leere Felder. Der Spieler am Zug ist entweder 1 (X) oder -1 (O).

```lua
function Position.new()
    local self = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}
    setmetatable(self, Position)
    self.player = 1
    return self
end
```

Wir schreiben außerdem eine Funktion, die eine Stellung kopiert und dabei den Spieler vertauscht. Wir werden sie gleich bei der Zuggenerierung brauchen.

```lua
function Position:copy()
    local copy = {{}, {}, {}}
    setmetatable(copy, Position)
    for i = 1, 3 do
        for j = 1, 3 do
            copy[i][j] = self[i][j]
        end
    end
    copy.player = -self.player
    return copy
end
```

Die Funktion `generatemoves` erzeugt für jeden gültigen Zug eine Kopie der Stellung und führt den Zug auf der Kopie aus.

```lua
function Position:generatemoves()
    local moves = {}
    for i = 1, 3 do
        for j = 1, 3 do
            if self[i][j] == 0 then
                local move = self:copy()
                move[i][j] = self.player
                move.notation = tostring(9 - 3 * i + j)
                table.insert(moves, move)
            end
        end
    end
    return moves
end
```

(`notation` werden wir später für die Eingabe menschlicher Züge benötigen.)

Die Blätter des Spielbaums können wir mit einer Punktzahl bewerten: 1 wenn Spieler X gewonnen hat, -1 wenn Spieler O gewonnen hat und 0 wenn das Spiel unentschieden ausgegangen ist. Die Funktion `findthreeinarow` durchsucht die Stellung systematisch nach Dreierreihen und gibt die entsprechende Punktzahl zurück.

```lua
function Position:findthreeinarow()
    for i = 1, 3 do
        if self[i][1] ~= 0 then
            if self[i][2] == self[i][1] and self[i][3] == self[i][1] then
                return self[i][1]
            end
        end
        if self[1][i] ~= 0 then
            if self[2][i] == self[1][i] and self[3][i] == self[1][i] then
                return self[1][i]
            end
        end
    end
    if self[2][2] ~= 0 then
        if self[1][1] == self[2][2] and self[3][3] == self[2][2] then
            return self[2][2]
        elseif self[1][3] == self[2][2] and self[3][1] == self[2][2] then
            return self[2][2]
        end
    end
    return 0
end
```

Um uns zu überlegen, wie wir die Nicht-Blätter bewerten, schauen wir uns zunächst einen Knoten an, der drei Blätter als Kinder hat. Diese Blätter haben die Punktzahlen 1, 0 und -1. Falls X am Zug ist, wird er den Zug wählen, der 1 ergibt (X versucht also die Punktzahl zu maximieren). Falls O am Zug ist, wählt er natürlich -1 (O minimiert). Wir bewerten diesen Knoten also entsprechend entweder mit der größten oder kleinsten Kindpunktzahl.

Dieselbe Überlegung gilt aber nicht nur für Knoten, deren Kinder Blätter sind, sondern allgemein für alle Knoten, deren Kinder schon bewertet worden sind. Wir müssen also Kinder immer vor ihren Eltern bewerten und dafür kommt eine Art Post-Order-Traversierung zum Einsatz. Dazu schreiben wir zwei Funktionen `maxi` und `mini`, die sich gegenseitig rekursiv aufrufen. `maxi` wird für Stellungen verwendet, in denen der maximierende Spieler am Zug ist, `mini` für den minimierenden Spieler. Beide Funktionen geben die Punktzahl und den besten Zug zurück. Die Vorgehensweise schauen wir uns nun anhand von `maxi` an, `mini` ist im Wesentlichen dasselbe mit umgekehrten Vorzeichen.

Falls die Stellung eine Dreierreihe enthält, können wir direkt ein Ergebnis zurückgeben:

```lua
local threeinarow = self:findthreeinarow()
if threeinarow ~= 0 then
    return threeinarow, nil
end
```

Wenn keine Züge mehr möglich sind, dann ist das Spiel unentschieden und wir können auch ein Ergebnis zurückgeben.

```lua
local moves = self:generatemoves()
if #moves == 0 then
    return 0, nil
end
```

Ansonsten bewerten wir die möglichen Züge und geben den mit der größten Punktzahl zurück (in `mini` den mit der kleinsten).

```lua
local bestscore = -1 / 0
local bestmove = nil
for _, move in ipairs(moves) do
    local score, _ = move:mini()
    if score > bestscore then
        bestscore, bestmove = score, move
    end
end
return bestscore, bestmove
```

`bestscore` ist zunächst -unendlich, damit bereits der erste Zug als (vorläufig) bester Zug gibt (in `mini` +unendlich).

Der menschliche Spieler gibt seine Züge mit dem Ziffernblock ein. In einer Endlosschleife lassen wir den Spieler Züge eingeben, bis er einen gültigen Zug eingegeben hat.

```lua
local moves = position:generatemoves()
local legal = false
while not legal do
    local humanmove = io.read()
    for _, move in ipairs(moves) do
        if move.notation == humanmove then
            position = move
            legal = true
            break
        end
    end
end
```

Der Computerspieler findet natürlich mit dem Minimax-Algorithmus seinen Zug:

```lua
_, position = position:mini()
```

Nach jedem Zug, egal ob Mensch oder Computer, prüfen wir, ob das Spiel zu Ende ist.

```lua
if position:findthreeinarow() ~= 0 or #position:generatemoves() == 0 then
    break
end
```
