## Sieb des Eratosthenes

*Code: [sieve.lua](../code/sieve.lua)*

Jede natürliche Zahl ist durch 1 und durch sich selbst teilbar. Wenn eine Zahl durch sonst nichts anderes teilbar ist, dann nennt man sie Primzahl. Beispiele sind 2, 3, 5, 7 und 11. Keine Primzahlen sind z.B. 4 (teilbar durch 2), 6 (teilbar durch 2 und 3), 8 (teilbar durch 2 und 4), 9 (teilbar durch 3) und 10 (teilbar durch 2 und 5). Die 1 ist ein Sonderfall, die per Festlegung nicht als Primzahl gilt.

Der Sieb des Eratosthenes ist ein antiker Algorithmus zur Ermittlung aller Primzahlen von 1 bis n. Die Vorgehensweise lautet so:

* Schreibe alle Zahlen von 1 bis n auf ein Blatt Papier.
* Streiche die 1.
* Gehe alle Zahlen i von 1 bis n durch:
  * Ist die Zahl gestrichen, ist sie keine Primzahl.
  * Ist sie nicht gestrichen, dann ist sie eine Primzahl. Streiche ihre Vielfachen beginnend mit i<sup>2</sup>.

Statt eines Blatts Papier nehmen wir ein Array von Booleans. Die Array-Indizes stehen für die einzelnen Zahlen, die Booleans sagen uns, ob die Zahl gestrichen wurde (true = nicht gestrichen).

```lua
local list = {}
for i = 1, 1000000 do
    table.insert(list, true)
end
```

Jetzt streichen wir die 1.

```lua
list[1] = false
```

In Sprachen, die Arrays ab 0 durchnummerieren, muss natürlich zusätzlich die 0 gestrichen werden.

Und jetzt gehen wir auf Primzahlensuche. In einer inneren Schleife streichen wir ggf. die Vielfachen.

```lua
for i, isprime in ipairs(list) do
    if isprime then
        print(i)
        for j = i^2, #list, i do
            list[j] = false
        end
    end
end
```
