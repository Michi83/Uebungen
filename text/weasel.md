## Dawkins' Wiesel

*Code: [weasel.lua](../code/weasel.lua)*

Der Biologe Richard Dawkins hat sich folgendes Programm ausgedacht um die Evolutionstheorie zu veranschaulichen:

1. Ein Zufallsstring der Länge 28 wird erzeugt. Er kann Großbuchstaben und Leerzeichen enthalten.
2. Von diesem String erzeugen wir 100 Kopien (Kinder), wobei jeder Buchstabe mit einer Wahrscheinlichkeit von 5% mutiert und durch einen zufälligen Buchstaben ersetzt wird. (Mit Buchstaben kann hier auch das Leerzeichen gemeint sein.)
3. Die Kinder werden mit dem Shakespeare-Zitat `"METHINKS IT IS LIKE A WEASEL"` verglichen. Für jeden Buchstaben an der richtigen Stelle erhält der String einen Punkt.
4. Das Kind mit den meisten Punkten wird Elternstring der nächsten Generation.

Im Laufe der Generationen werden die Strings dem Zielstring immer ähnlicher bis schließlich ein String mit dem Zielstring identisch ist.

Fangen wir mit ein paar wichtigen Variablen an:

```lua
local fertility = 100
local mutationrate = 5
local genepool =
{
    " ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
    "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
}
local target =
{
    "M", "E", "T", "H", "I", "N", "K", "S", " ", "I", "T", " ", "I", "S", " ",
    "L", "I", "K", "E", " ", "A", " ", "W", "E", "A", "S", "E", "L"
}
```

`fertility` ist die Anzahl der Kinder pro Generation, `mutationrate` die Mutationswahrscheinlichkeit pro Buchstabe in Prozent. `genepool` enthält die erlaubten Buchstaben und `target` das zu erreichende Ziel. Anstelle von Strings verwenden wir Arrays einzelner Zeichen und wandeln sie nur bei Bedarf in Strings um, da sich einige Operationen dadurch vereinfachen.

Jetzt erzeugen wir den ersten String:

```lua
local parent = {}
for i = 1, #target do
    table.insert(parent, genepool[math.random(#genepool)])
end
print(table.concat(parent))
```

Beim Erzeugen der einzelnen Kinder entscheidet der Zufall, zusammen mit der Mutationsrate, ob das Kind einen Buchstaben vom Elternstring erbt oder einen Zufallsbuchstaben erhält:

```lua
if math.random(100) > mutationrate then
    table.insert(child, parent[j])
else
    table.insert(child, genepool[math.random(#genepool)])
end
```

Und während wir das Kind erzeugen, zählen wir auch gleich dessen Punktzahl.

```lua
if child[j] == target[j] then
    score = score + 1
end
```

Wir merken uns immer das bisher beste Kind.

```lua
if bestscore == nil or score > bestscore then
    bestchild = child
    bestscore = score
end
```

Das beste Kind wird am Ende zum neuen Elternstring. Wenn dabei die maximale Punktzahl erreicht wurde, dann bricht die Schleife ab.

```lua
parent = bestchild
print(table.concat(parent))
if bestscore == #target then
    break
end
```

Beispielausgabe:

```
VJUUXEITGNLPIMYXQSCP FCUDJCB
VJUUXEITGNLPIMYLQSCP FCUDJGB
VJUUXEITCNLPIMYLQSCP FCBAJGB
VJUUXEITCNLPIMYLQSCPAFCBAJGB
VJUUXEITCNLPIMYLQSCPA CBAJGB
VVUUXEITCNLPIHYLLKCPA CBAJGB
VVTUXEITCNGPIHYLLKCCA CBAJGB
VVTUXEJTCNGPIHYLLKCCA WBAJGB
VVTUXEJTCNGPIHYLIKCCA WBAJGB
VVTUIEJTCNGPIHYLIKCCA WBAJGB
VVTUIEJTCNGPIH LIKCCA WBAJGB
MVTUIEJTCNGPIH LIKCCA WBAJGB
MVTUIEJTCNGPIJ LIKCCA WBAJGB
MVTUIEJTCNGPIJ LIKC A WBAJGB
MVTUIEJTCNGPIJ LIKC A WBAJGB
MVTUIEJTCIGPIJ LIKC A WBAJKB
MVTUIEKTUIMPIJ LIKZ A WBAJKB
MVTUIEKTUIMPIJ LIKZ A WBAJKB
MVTUINKTUIMPIJ LIKZ A WBAJXB
METUINKTUIMPIR LIKZ A WBAJXB
METMINKTUIMPIR LIKZ A WBADXB
METMINKTUIMPIR LIKZ A WBADXB
METMINKTUIM IR LIKZ A WBADXB
METMIN TUIM IR LIKZ A WBADXL
METMIN TUIM IR LIKZ A WBADXL
METMIN T IM IR LIKZ A WBADXL
METMIN T IM IR LIKZ A WBADXL
METHIN T IM IR LIKZ A WBADXL
METHIN T IM IR LIKZ A WBADXL
METHIN T IM IR LIKZ A WBADXL
METHIN S IH IR LIKF A WBADXL
METHIN S IF IR LIKF A WBADXL
METHIN S IF IR LIKF A WBADOL
METHIN S IT IR LIKF A WBADOL
METHINXS IT IR LIKM A WEADOL
METHINXS IT IR LIKM A WEADOL
METHINXS IT IR LIKM A WEADOL
METHINXS IT IR LIKM A WEADOL
METHINXS IT IR LIKM A WEADOL
METHINSS IT IR LIKM A WEADOL
METHINSS IT IR LIKM A WEADOL
METHINSS IT IR LIKM A WEADWL
METHINKS IT IA LIKM A WEADWL
METHINKS IT IA LIKM A WEAAWL
METHINKS IT IA LIKM A WEAAWL
METHINKS IT IA LIKM A WEASWL
METHINKS IT IA LIKE A WEASUL
METHINKS IT IA LIKE A WEASUL
METHINKS IT IA LIKE A WEASUL
METHINKS IT IA LIKE A WEASUL
METHINKS IT IA LIKE A WEASUL
METHINKS IT IA LIKE A WEASUL
METHINKS IT IA LIKE A WEASUL
METHINKS IT IA LIKE A WEASUL
METHINKS IT IA LIKE A WEASOL
METHINKS IT IA LIKE A WEASOL
METHINKS IT IS LIKE A WEASOL
METHINKS IT IS LIKE A WEASOL
METHINKS IT IS LIKE A WEASOL
METHINKS IT IS LIKE A WEASOL
METHINKS IT IS LIKE A WEASOL
METHINKS IT IS LIKE A WEASOL
METHINKS IT IS LIKE A WEASEL
```
