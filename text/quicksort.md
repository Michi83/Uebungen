## Quicksort

*Code: [quicksort.lua](../code/quicksort.lua)*

Eine weitere Methode, Arrays zu sortieren, ist Quicksort. Ohne gleich ans Programmieren zu denken, können wir die Vorgehensweise so beschreiben: Wir haben einen Stapel Karten mit Zahlen darauf. Wir wählen eine beliebige Karte aus (wir nennen sie Drehpunkt oder engl. "Pivot") und legen sie in die Mitte des Tisches. Alle Karten kleiner/gleich dem Pivot legen wir links davon auf einen Stapel und alle Karten, die größer sind, rechts. Jetzt haben wir das Problem bereits in zwei kleinere Teilprobleme zerlegt und können sie mit derselben Methode weiter zerlegen bis am Ende alles sortiert ist.

Das mehrfache Anwenden dieser Methode wird auf eine Rekursion in Teilbereichen des Arrays hinauslaufen. Wir definieren deshalb zwei Extraparameter, `left` und `right`, die den zu bearbeitenden Bereich abgrenzen. Für beide geben wir Standardwerte vor, so dass beim Fehlen der Parameter das ganze Array bearbeitet wird.

```lua
left = left or 1
right = right or #array
```

Bei rekursiven Funktionen besteht oft die Gefahr, dass man sich in einer unendlichen Rekursion verheddert. Auch hier kann das passieren, wenn wir versuchen einen Bereich mit weniger als zwei Elementen noch weiter zu zerlegen. Davor schützen wir uns, indem wir in so einem Fall die Funktion sofort abbrechen.

```lua
if not (right > left) then
    return
end
```

Den Pivot können wir beliebig wählen, wir nehmen einfach das letzte Element des Bereichs.

```lua
local pivot = array[right]
```

Jetzt müssen wir den Rest des Bereichs in einen linken und rechten Teilbereich zerlegen. Dazu verwenden wir zwei Hilfsvariablen `i` und `j`, die zunächst den Beginn des Bereichs markieren. `j` durchläuft dann in einer Schleife den ganzen Bereich einschließlich des Pivot und immer wenn ein Element kleiner/gleich dem Pivot ist, wird es an die Stelle `i` verschoben und `i` um 1 erhöht.

```lua
local i = left
for j = left, right do
    if array[j] <= pivot then
        array[i], array[j] = array[j], array[i]
        i = i + 1
    end
end
```

Der Bereich zerfällt dadurch in drei Teilbereiche:

* Zwischen `left` und `i`: Die Zahlen kleiner/gleich dem Pivot.
* Zwischen `i` und `j`: Die Zahlen größer als der Pivot.
* Zwischen `j` und `right`: Die noch zu untersuchenden Zahlen.

Der letzte Bereich wird natürlich immer kleiner und verschwindet schließlich. Da auch der Pivot kleiner/gleich dem Pivot ist, wird er am Ende der Schleife in die Mitte verschoben. `i` befindet sich dann eine Stelle hinter dem Pivot und markiert den Beginn des rechten Teilbereichs, `i - 2` das Ende des linken.

Und damit können wir jetzt in die Rekursion gehen:

```lua
quicksort(array, left, i - 2)
quicksort(array, i, right)
```
