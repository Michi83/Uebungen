## Verhältniswahl

*Code: [election.lua](../code/election.lua)*

Bei einer Verhältniswahl erhält jede Partei einen Anteil an den Sitzen, der ihren Anteil an den Stimmen entspricht. Hat z.B. eine Partei 10% der Stimmen erhalten, dann erhält sie auch 10% der Sitze. Das muss natürlich nach irgendeinem System auf- oder abgerundet werden und ein solches System ist das d'Hondt-Verfahren.

Für unser Beispiel verwenden wir das Ergebnis der Bundestagswahl 2013:

```
CDU,14921877
SPD,11252215
Linke,3755699
Grüne,3694057
CSU,3243569
FDP,2083533
AfD,2056985
Piraten,959177
NPD,560828
Freie Wähler,423977
Tierschutzpartei,140366
ÖDP,127088
Republikaner,91193
Die PARTEI,78674
pro Deutschland,73854
BP,57395
Volksabstimmung,28654
Rentnerpartei,25134
Partei der Vernunft,24719
MLPD,24219
PBC,18542
BIG,17743
BüSo,12814
Frauenpartei,12148
Nichtwählerpartei,11349
RRP,8578
Violette,8211
Familienpartei,7449
PSG,4564
Rechte,2245
```

Beim d'Hondt-Verfahren wird eine Tabelle aufgestellt: In den Spalten die Parteien, in den Zeilen die Zahlen 1 bis n (wobei n die Zahl der zu verteilenden Sitze ist). In der Tabelle teilen wir die Stimmen der einzelnen Parteien durch 1 bis n.

```
    CDU      SPD       Linke    Grüne    CSU     usw...
  1 14921877 11252215  3755699  3694057  3243569
  2  7460938  5626108  1877850  1847028  1621784
  3  4973959  3750738  1251900  1231352  1081190
  4  3730469  2813054   938925   923514   810892
  5  2984375  2250443   751140   738811   648714
usw...
```

Wir suchen nun in der Tabelle die n größten Zahlen und teilen der zugehörigen Partei jeweils einen Sitz zu.

Als Programm setzen wir das d'Hondt-Verfahren so um: Als erstes lesen wir das Wahlergebnis aus einer Datei ein.

```lua
local parties = {}

for line in io.lines("election.csv") do
    local separator = line:find(",")
    local name = line:sub(1, separator - 1)
    local votes = tonumber(line:sub(separator + 1, #line))
    table.insert(parties, {name = name, votes = votes, divisor = 1})
end
```

Jeder Partei weisen wir einen Divisor zu, der zunächst für jede Partei 1 beträgt. Jetzt suchen wir n mal die Partei, die aktuell die größte Zahl (Stimmen / Divisor) hat und erhöhen ihren Divisor um 1.

```lua
local bestnumber = -1 / 0
local bestparty = nil
for _, party in ipairs(parties) do
    local number = party.votes / party.divisor
    if number > bestnumber then
        bestnumber = number
        bestparty = party
    end
end
bestparty.divisor = bestparty.divisor + 1
```

Die Sitze jeder Partei betragen nun Divisor - 1.

```lua
for _, party in ipairs(parties) do
    print(("%3d %s"):format(party.divisor - 1, party.name))
end
```
