## Poker

*Code: [poker.lua](../code/poker.lua)*

Beim Poker versuchen die Spieler möglichst wertvolle Kombinationen von fünf Karten zu bilden. Folgende Kombinationsmöglichkeiten gibt es:

* **Straight flush**: Fünf Karten mit aufeinanderfolgenden Werten in derselben Farbe, z.B. A&spades; K&spades; D&spades; B&spades; 10&spades;. Das Ass kann dabei auch als niedrigste Karte zählen, z.B. 5&spades; 4&spades; 3&spades; 2&spades; A&spades;
* **Four of a kind**: Vier Karten desselben Wertes und eine beliebige fünfte Karte, z.B. A&spades; A&hearts; A&diams; A&clubs; K&spades;.
* **Full house**: Drei Karten eines Wertes und zwei Karten eines anderen Wertes, z.B. A&spades; A&hearts; A&diams; K&spades; K&hearts;.
* **Flush**: Fünf Karten derselben Farbe, z.B. A&spades; D&spades; 10&spades; 8&spades; 6&spades;.
* **Straight**: Fünf Karten mit aufeinanderfolgenden Werten aber verschiedenen Farben, z.B. A&spades; K&hearts; D&diams; B&clubs; 10&spades;. Wie beim Straight flush kann das Ass auch als niedrigste Karte zählen.
* **Three of a kind**: Drei Karten desselben Wertes und zwei beliebige Zusatzkarten, z.B. A&spades; A&hearts; A&diams; K&spades; D&spades;
* **Two pair**: Zweimal zwei Karten desselben Wertes, z.B. A&spades; A&hearts; K&spades; K&hearts; D&spades;
* **One pair**: Zwei Karten desselben Wertes, z.B. A&spades; A&hearts; K&spades; D&spades; B&spades;
* **High card**: Alle anderen Kombinationen.

Schreiben wir mal eine Funktion, die uns für beliebige fünf Karten sagt, welche Art von Kombination es ist. Fangen wir mit ein paar nützlichen symbolischen Konstanten an:

```lua
local STRAIGHT_FLUSH = 9
local FOUR_OF_A_KIND = 8
local FULL_HOUSE = 7
local FLUSH = 6
local STRAIGHT = 5
local THREE_OF_A_KIND = 4
local TWO_PAIR = 3
local ONE_PAIR = 2
local HIGH_CARD = 1

local ACE = 14
local KING = 13
local QUEEN = 12
local JACK = 11

local SPADES = 1
local HEARTS = 2
local DIAMONDS = 3
local CLUBS = 4
```

Als erstes machen wir "Inventur". Wir zählen, wie oft die einzelnen Werte und Farben vorkommen.

```lua
local ranks = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
local suits = {0, 0, 0, 0}
for _, card in ipairs(hand) do
    ranks[card.rank] = ranks[card.rank] + 1
    suits[card.suit] = suits[card.suit] + 1
end
```

Und da Asse unter Umständen auch als niedrigste Karte zählen können:

```lua
ranks[1] = ranks[ACE]
```

Jetzt schauen wir mal ob ein Straight oder ein Flush (oder beides) vorhanden ist.

```lua
local straight = false
local flush = false
for i = ACE, 5, -1 do
    if ranks[i] == 1 and ranks[i - 1] == 1 and ranks[i - 2] == 1
    and ranks[i - 3] == 1 and ranks[i - 4] == 1 then
        straight = true
        break
    end
end
for _, suit in ipairs(suits) do
    if suit == 5 then
        flush = true
        break
    end
end
if straight and flush then
    return STRAIGHT_FLUSH
elseif flush then
    return FLUSH
elseif straight then
    return STRAIGHT
end
```

Jetzt prüfen wir ob Werte mehrfach vorkommen.

```lua
local threeofakind = false
local pairs = 0
for i = ACE, 2, -1 do
    if ranks[i] == 4 then
        return FOUR_OF_A_KIND
    elseif ranks[i] == 3 then
        threeofakind = true
    elseif ranks[i] == 2 then
        pairs = pairs + 1
    end
end
if threeofakind and pairs == 1 then
    return FULL_HOUSE
elseif threeofakind then
    return THREE_OF_A_KIND
elseif pairs == 2 then
    return TWO_PAIR
elseif pairs == 1 then
    return ONE_PAIR
else
    return HIGH_CARD
end
```

Jetzt erzeugen wir ein Array, das einen vollständigen Satz Karten enthält:

```lua
local deck = {}
for rank = ACE, 2, -1 do
    for suit = SPADES, CLUBS do
        local card = {rank = rank, suit = suit}
        table.insert(deck, card)
    end
end
```

Und jetzt machen wir den Härtetest! Wir gehen alle möglichen Kombinationen von fünf Karten durch und zählen, wie oft die einzelnen Kombinationsarten vorkommen.

```lua
local handtypes = {0, 0, 0, 0, 0, 0, 0, 0, 0}
for i = 1, #deck do
    for j = i + 1, #deck do
        for k = j + 1, #deck do
            for l = k + 1, #deck do
                for m = l + 1, #deck do
                    local hand = {deck[i], deck[j], deck[k], deck[l], deck[m]}
                    local handtype = evaluate(hand)
                    handtypes[handtype] = handtypes[handtype] + 1
                end
            end
        end
    end
end
print("Straight flush:  " .. handtypes[STRAIGHT_FLUSH])
print("Four of a kind:  " .. handtypes[FOUR_OF_A_KIND])
print("Full house:      " .. handtypes[FULL_HOUSE])
print("Flush:           " .. handtypes[FLUSH])
print("Straight:        " .. handtypes[STRAIGHT])
print("Three of a kind: " .. handtypes[THREE_OF_A_KIND])
print("Two pair:        " .. handtypes[TWO_PAIR])
print("One pair:        " .. handtypes[ONE_PAIR])
print("High card:       " .. handtypes[HIGH_CARD])
```

Es wird wahrscheinlich ein bisschen dauern, bis dieser Code durch ist. Das Ergebnis sollte dann aber so lauten:

```
Straight flush:  40
Four of a kind:  624
Full house:      3744
Flush:           5108
Straight:        10200
Three of a kind: 54912
Two pair:        123552
One pair:        1098240
High card:       1302540
```

Wenn unser Ergebnis nur um 1 abweichen sollte, dann muss noch irgendwo ein Bug sein, den wir finden müssen.
