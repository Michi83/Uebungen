## Fußball-Spielplan

*Code: [football.lua](../code/football.lua)*

Mit folgender Methode kann man einen Spielplan für eine Fußball-Liga aufstellen:

* Der erste Spieltag wird beliebig eingeteilt.
* Eine Mannschaft im ersten oder letzten Spiel ist "fest" und befindet sich an allen Spieltagen an derselben Stelle.
* Alle anderen Mannschaften rotieren pro Spieltag um eine Stelle weiter.

Beispiel mit sechs Mannschaften (A ist fest, alle anderen rotieren gegen den Uhrzeigersinn):

| 1. Spieltag               | 2. Spieltag               | 3. Spieltag               | 4. Spieltag               | 5. Spieltag               |
| :-----------------------: | :-----------------------: | :-----------------------: | :-----------------------: | :-----------------------: |
| A - F<br/>B - E<br/>C - D | A - E<br/>F - D<br/>B - C | A - D<br/>E - C<br/>F - B | A - C<br/>D - B<br/>E - F | A - B<br/>C - F<br/>D - E |

Codebeispiel:

```lua
local teams = {"A", "B", "C", "D", "E", "F"}

for matchday = 1, #teams - 1 do
    for i = 1, #teams / 2 do
        print(teams[i] .. " - " .. teams[#teams - i + 1])
    end
    print()
    local temp = teams[#teams]
    for i = #teams, 3, -1 do
        teams[i] = teams[i - 1]
    end
    teams[2] = temp
end
```

Wir haben also ein Array von Mannschaften und lassen immer die erste gegen die letzte, die zweite gegen die vorletzte, die dritte gegen die drittletzte usw... spielen:

```lua
for i = 1, #teams / 2 do
    print(teams[i] .. " - " .. teams[#teams - i + 1])
end
```

(Wir können uns vorstellen, dass das Array in der Mitte "gefaltet" ist, wie ein Blatt Papier, so dass die Mannschaften, die gegeneinander spielen, zusammen liegen.)

Dann rotieren wir die Mannschaften, indem wir alle bis auf die erste um eine Stelle nach hinten verschieben. Die letzte verschieben wir nach vorn an die zweite Stelle:

```lua
local temp = teams[#teams]
for i = #teams, 3, -1 do
    teams[i] = teams[i - 1]
end
teams[2] = temp
end
```

(Auch hier hilft evtl. wieder die Vorstellung vom gefalteten Array.)

Diese Methode lässt sich übrigens auch auf eine ungerade Anzahl von Mannschaften anwenden, wenn wir noch eine "Geistermannschaft" hinzufügen. Die Mannschaft, die gegen die Geistermannschaft eingeteilt ist, hat spielfrei.
