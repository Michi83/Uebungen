## Binäre Suche

*Code: [binarysearch.lua](../code/binarysearch.lua)*

Wenn wir in einem Array ein bestimmtes Element finden wollen, dann bleibt uns meist nichts anderes übrig, als das Array in einer Schleife durchzugehen, bis wir das Element gefunden haben. Wenn aber die Elemente der Größe nach sortiert sind, dann können wir eine binäre Suche anwenden. Die Vorgehensweise ist so ähnlich wie die Suche in einem alphabetisch sortierten Telefonbuch. Wir schauen uns das mittlere Element an. Ist es größer als das gesuchte Element, dann muss es sich in der vorderen Hälfte befinden (unter der Annahme, dass die Elemente vom kleinsten bis zum größten sortiert sind), ansonsten in der hinteren Hälfte. Wir können dann den Suchbereich immer weiter halbieren, bis wir das Element gefunden haben.

`left` und `right` grenzen dem Suchbereich ein, zu Beginn das ganze Array.

```lua
local left = 1
local right = #haystack
```

Die Mitte ist der Durchschnitt von `left` und `right`, ggf. auf eine ganze Zahl gerundet.

```lua
local middle = math.floor((left + right) / 2)
```

(Die genaue Rundungsregel spielt keine Rolle. Wir runden hier immer ab.)

Ist die Mitte des Heuhaufens größer als die Nadel, dann können wir die ganze rechte Hälfte einschließlich der Mitte ausschließen. Ist die Mitte kleiner als die Nadel dann ist es genau umgekehrt. Ansonsten haben wir die Nadel gefunden und können ihren Index als Ergebnis zurückgeben.

```lua
if haystack[middle] > needle then
    right = middle - 1
elseif haystack[middle] < needle then
    left = middle + 1
else
    return middle
end
```

Wenn der Suchbereich komplett "weggeschmolzen" ist, ohne dass wir die Nadel gefunden haben, dann ist sie nicht im Array enthalten und wir geben `nil` zurück.

```lua
while left <= right do
    -- ...
end
return nil
```
