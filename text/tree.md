## Baumstrukturen

*Code: [tree.lua](../code/tree.lua)*

Eine Baumstruktur ist eine Datenstruktur, in der die einzelnen Datenelemente, Knoten genannt, hierarchisch angeordnet sind. Jeder Konten hat einen Elternknoten und kann mehrere Kindknoten haben. An der Spitze befindet sich der Wurzelknoten, der als einziger keinen Elternknoten hat. Knoten ohne Kinder werden Blätter genannt. Das Ganze kann man sich also wie einen auf dem Kopf stehenden Baum vorstellen.

![Baumstruktur](tree.png)

Beispiele für Baumstrukturen:
* In einem Unternehmen hat jeder Mitarbeiter einen Vorgesetzten und kann mehrere Untergebene haben. Ganz oben steht der Chef, der keinen Vorgesetzten hat.
* Die Ordner und Dateien eines Dateisystems bilden eine Baumstruktur, da jeder Ordner Unterordner enthalten kann.
* Eine HTML-Datei kann man sich als Baumstruktur vorstellen. Die Wurzel ist das `html`-Element, dessen Kinder sind `head` und `body` und diese können selbst wieder Kindknoten enthalten.

Bei der Baumtraversierung geht es darum, alle Elemente eines Baums der Reihe nach zu besuchen und zu bearbeiten. Bei eine Datenstruktur wie dem Array oder der [verknüpften Liste](linkedlist.md) wäre das ganz einfach: Wir gehen die Elemente einfach in einer Schleife durch. Bei Baumstrukturen müssen wir aber andere Methoden anwenden. Drei davon schauen wir uns gleich an. Aber zunächst bauen wir die Baumstruktur aus dem Diagramm auf:

```lua
local node1 = {value = 1, children = {}}
local node2 = {value = 2, children = {}}
local node3 = {value = 3, children = {}}
local node4 = {value = 4, children = {}}
local node5 = {value = 5, children = {}}
local node6 = {value = 6, children = {}}
local node7 = {value = 7, children = {}}
local node8 = {value = 8, children = {}}
local node9 = {value = 9, children = {}}
local node10 = {value = 10, children = {}}
local node11 = {value = 11, children = {}}
local node12 = {value = 12, children = {}}
local node13 = {value = 13, children = {}}
```

Jetzt existieren zwar schon die Knoten, stehen aber noch in keiner Beziehung zueinander. Es fehlt noch die Information, welche Knoten ein Kind von welchem ist.

```lua
table.insert(node1.children, node2)
table.insert(node1.children, node3)
table.insert(node1.children, node4)
table.insert(node2.children, node5)
table.insert(node2.children, node6)
table.insert(node2.children, node7)
table.insert(node3.children, node8)
table.insert(node3.children, node9)
table.insert(node3.children, node10)
table.insert(node4.children, node11)
table.insert(node4.children, node12)
table.insert(node4.children, node13)
```

Bei der Pre-Order-Traversierung kommt eine Funktion zum Einsatz, die zunächst einen Knoten bearbeitet und sich dann selbst rekursiv für dessen Kinder aufruft. Durch die Rekursion werden dann auch die Kinder der Kinder, die Kinder der Kinder der Kinder usw. bearbeitet.

```lua
local function preorder(node)
    print(node.value)
    for _, child in ipairs(node.children) do
        preorder(child)
    end
end
```

(In diesem Beispiel besteht die Bearbeitung nur darin, den Wert auszugeben. Tatsächlich kann hier natürlich jede beliebige Bearbeitung stattfinden.)

Wir setzen das Ganze in Gang, indem wir die Funktion mit der Wurzel aufrufen:

```lua
preorder(node1)
```

Ganz ähnlich funktioniert die Post-Order-Traversierung. Hier findet allerdings die Rekursion vor der Bearbeitung statt.

```lua
local function postorder(node)
    for _, child in ipairs(node.children) do
        postorder(child)
    end
    print(node.value)
end
```

Bei der Level-Order-Traversierung legen wir eine [Queue](deque.md) an und fügen ihr zunächst die Wurzel hinzu. Solange die Queue Elemente enthält, entnehmen wir immer wieder einen Knoten, bearbeiten ihn, und fügen seine Kinder der Queue hinzu.

```lua
local function levelorder(root)
    local queue = {}
    table.insert(queue, root)
    while #queue > 0 do
        local node = table.remove(queue, 1)
        print(node.value)
        for _, child in ipairs(node.children) do
            table.insert(queue, child)
        end
    end
end
```

Der entscheidende Unterschied dieser Vorgehensweisen ist die Reihenfolge, in der die Knoten abgearbeitet werden:
* Bei Pre-Order werden immer Eltern vor ihren Kinder bearbeitet.
* Bei Post-Order werden immer Kinder vor ihren Eltern bearbeitet.
* Bei Level-Order werden, wie bei Pre-Order, Eltern vor ihren Kindern bearbeitet. Noch dazu werden die Knoten ebenenweise abgearbeitet.
