## Zufallsgenerator

*Code: [random.lua](../code/random.lua)*

Ein Pseudo-Zufallsgenerator (pseudo-random number generator, PRNG) ist ein Algorithmus, der eine Folge von Zahlen erzeugt, die zufällig aussieht, tatsächlich aber berechnet sind. Bei genauer Analyse findet man natürlich Regelmäßigkeiten, die in echten Zufallszahlen nicht auftreten würden. Das soll uns aber nicht kümmern, für uns zählt nur, dass ein PRNG eine interessante Übung ist.

Ein einfacher PRNG ist der Lehmer-Generator, der auf folgender Formel basiert:

x<sub>n+1</sub> = a * x<sub>n</sub> % b

Dabe sind x<sub>n</sub> und x<sub>n+1</sub> Zufallszahlen und a und b beliebige Konstanten. In Worten bedeutet das: Wenn wir eine Zufallszahl x<sub>n</sub> haben, dann können wir sie in Formel einsetzen und erhalten eine weitere Zufallszahl x<sub>n+1</sub>. Die können dann wieder in die Formel einsetzen und erhalten noch eine Zufallszahl usw...

Die Zahlen a und b können wir beliebig wählen, aber wenn wir schlechte Zahlen nehmen, dann werden wir schlechte Zufallszahlen erhalten. Zum Beispiel könnte sich die Folge der Zahlen nach kurzer Zeit wiederholen. Wir werden uns hier aber nicht zu sehr in die Mathematik vertiefen und nehmen einfach die Zahlen, die Stephen Park und Keith Miller empfehlen (http://www.firstpr.com.au/dsp/rand31/p105-crawford.pdf):

```
a = 48271
b = 2147483647
```

Jetzt fehlt uns nur noch ein Startwert x<sub>0</sub>, auch Seed genannt, um das ganze in Gang  zu setzen. Für erste Tests nehmen wir x<sub>0</sub> = 1, später sollten wir den Seed von der Uhrzeit abhängig machen, damit bei jedem Programmstart andere Zufallszahlen erzeugt werden.

```lua
local seed = 1

local function random()
    seed = 48271 * seed % 2147483647
    return seed
end
```

Die ersten zehn damit erzeugten Zahlen lauten:

```
48271
182605794
1291394886
1914720637
2078669041
407355683
1105902161
854716505
564586691
1596680831
```

Diese Zahlen können wir mit dem Modulo-Operator in einen kleineren Bereich "runterrechnen". Bei x % y kommt nämlich immer eine Zahl im Bereich von 0 bis y - 1 heraus. Wenn wir die Zufallszahlen z.B. modulo 37 nehmen, dann kommen Zahlen im Bereich von 0 bis 36 heraus und könnten damit ein Roulette-Spiel programmieren.
