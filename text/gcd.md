## Größter gemeinsamer Teiler

*Code: [gcd.lua](../code/gcd.lua)*

Den größten gemeinsamen Teiler (ggT) zweier Zahlen a und b kann man mit Euklids Algorithmus berechnen. Er beruht auf folgenden Regeln:

1. Ist b gleich 0, dann ist a der ggT.
2. Der ggT von a und b ist gleich dem ggT von b und a % b.

Wenn wir die zweite Regel mehrfach anwenden, dann kommen wir irgendwann in die Lage, die erste Regel anwenden zu können. Das können wir z.B. in einer Schleife machen:

```lua
local function euclidloop(a, b)
    while b ~= 0 do
        a, b = b, a % b
    end
    return a
end
```

Alternativ lässt sich das auch mit einer rekursiven Funktion lösen:

```lua
local function euclidrecursive(a, b)
    if b == 0 then
        return a
    else
        return euclidrecursive(b, a % b)
    end
end
```
