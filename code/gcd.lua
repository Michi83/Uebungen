local function euclidloop(a, b)
    while b ~= 0 do
        a, b = b, a % b
    end
    return a
end

local function euclidrecursive(a, b)
    if b == 0 then
        return a
    else
        return euclidrecursive(b, a % b)
    end
end

io.write "a = "
local a = io.read "*number"
io.write "b = "
local b = io.read "*number"
print(euclidloop(a, b))
print(euclidrecursive(a, b))
