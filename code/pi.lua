local pi = 0
for i = 1, 1000000 do
    local fraction = 4 / (2 * i - 1)
    if i % 2 == 1 then
        pi = pi + fraction
    else
        pi = pi - fraction
    end
end
print(pi)
