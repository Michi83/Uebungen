local function newdeque()
    local deque = {}
    local head = {}
    head.next = head
    head.prev = head
    local length = 0

    function deque.getlength()
        return length
    end

    function deque.pushfront(item)
        local cell = {item = item, prev = head, next = head.next}
        cell.next.prev = cell
        cell.prev.next = cell
        length = length + 1
    end

    function deque.popfront()
        if length == 0 then return nil end
        local cell = head.next
        cell.next.prev = cell.prev
        cell.prev.next = cell.next
        length = length - 1
        return cell.item
    end

    function deque.pushback(item)
        local cell = {item = item, next = head, prev = head.prev}
        cell.next.prev = cell
        cell.prev.next = cell
        length = length + 1
    end

    function deque.popback()
        if length == 0 then return nil end
        local cell = head.prev
        cell.next.prev = cell.prev
        cell.prev.next = cell.next
        length = length - 1
        return cell.item
    end

    return deque
end
