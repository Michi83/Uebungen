local function quicksort(array, left, right)
    left = left or 1
    right = right or #array
    if not (right > left) then
        return
    end
    local pivot = array[right]
    local i = left
    for j = left, right do
        if array[j] <= pivot then
            array[i], array[j] = array[j], array[i]
            i = i + 1
        end
    end
    quicksort(array, left, i - 2)
    quicksort(array, i, right)
end
