local function newlinkedlist()
    local list = {first = nil, length = 0}

    function list.delete(index)
        if index == 1 then
            list.first = list.first.next
        else
            local cell = list.first
            for i = 1, index - 2 do
                cell = cell.next
            end
            cell.next = cell.next.next
        end
        list.length = list.length - 1
    end

    function list.get(index)
        local cell = list.first
        for i = 1, index - 1 do
            cell = cell.next
        end
        return cell.item
    end

    function list.insertafter(index, item)
        local cell = list.first
        for i = 1, index - 1 do
            cell = cell.next
        end
        local newcell = {item = item, next = cell.next}
        cell.next = newcell
        list.length = list.length + 1
    end

    function list.prepend(item)
        local cell = {item = item, next = list.first}
        list.first = cell
        list.length = list.length + 1
    end

    function list.set(index, item)
        local cell = list.first
        for i = 1, index - 1 do
            cell = cell.next
        end
        cell.item = item
    end

    return list
end
