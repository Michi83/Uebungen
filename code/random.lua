local seed = os.time()

local function random()
    seed = 48271 * seed % 2147483647
    return seed
end

return random
