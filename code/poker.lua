local STRAIGHT_FLUSH = 9
local FOUR_OF_A_KIND = 8
local FULL_HOUSE = 7
local FLUSH = 6
local STRAIGHT = 5
local THREE_OF_A_KIND = 4
local TWO_PAIR = 3
local ONE_PAIR = 2
local HIGH_CARD = 1

local ACE = 14
local KING = 13
local QUEEN = 12
local JACK = 11

local SPADES = 1
local HEARTS = 2
local DIAMONDS = 3
local CLUBS = 4

local function evaluate(hand)
    local ranks = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    local suits = {0, 0, 0, 0}
    for _, card in ipairs(hand) do
        ranks[card.rank] = ranks[card.rank] + 1
        suits[card.suit] = suits[card.suit] + 1
    end
    ranks[1] = ranks[ACE]
    local straight = false
    local flush = false
    for i = ACE, 5, -1 do
        if ranks[i] == 1 and ranks[i - 1] == 1 and ranks[i - 2] == 1
        and ranks[i - 3] == 1 and ranks[i - 4] == 1 then
            straight = true
            break
        end
    end
    for _, suit in ipairs(suits) do
        if suit == 5 then
            flush = true
            break
        end
    end
    if straight and flush then
        return STRAIGHT_FLUSH
    elseif flush then
        return FLUSH
    elseif straight then
        return STRAIGHT
    end
    local threeofakind = false
    local pairs = 0
    for i = ACE, 2, -1 do
        if ranks[i] == 4 then
            return FOUR_OF_A_KIND
        elseif ranks[i] == 3 then
            threeofakind = true
        elseif ranks[i] == 2 then
            pairs = pairs + 1
        end
    end
    if threeofakind and pairs == 1 then
        return FULL_HOUSE
    elseif threeofakind then
        return THREE_OF_A_KIND
    elseif pairs == 2 then
        return TWO_PAIR
    elseif pairs == 1 then
        return ONE_PAIR
    else
        return HIGH_CARD
    end
end

local deck = {}
for rank = ACE, 2, -1 do
    for suit = SPADES, CLUBS do
        local card = {rank = rank, suit = suit}
        table.insert(deck, card)
    end
end
local handtypes = {0, 0, 0, 0, 0, 0, 0, 0, 0}
for i = 1, #deck do
    for j = i + 1, #deck do
        for k = j + 1, #deck do
            for l = k + 1, #deck do
                for m = l + 1, #deck do
                    local hand = {deck[i], deck[j], deck[k], deck[l], deck[m]}
                    local handtype = evaluate(hand)
                    handtypes[handtype] = handtypes[handtype] + 1
                end
            end
        end
    end
end
print("Straight flush:  " .. handtypes[STRAIGHT_FLUSH])
print("Four of a kind:  " .. handtypes[FOUR_OF_A_KIND])
print("Full house:      " .. handtypes[FULL_HOUSE])
print("Flush:           " .. handtypes[FLUSH])
print("Straight:        " .. handtypes[STRAIGHT])
print("Three of a kind: " .. handtypes[THREE_OF_A_KIND])
print("Two pair:        " .. handtypes[TWO_PAIR])
print("One pair:        " .. handtypes[ONE_PAIR])
print("High card:       " .. handtypes[HIGH_CARD])
