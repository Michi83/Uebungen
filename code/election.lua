local parties = {}

for line in io.lines("election.csv") do
    local separator = line:find(",")
    local name = line:sub(1, separator - 1)
    local votes = tonumber(line:sub(separator + 1, #line))
    table.insert(parties, {name = name, votes = votes, divisor = 1})
end

for i = 1, 631 do
    local bestnumber = -1 / 0
    local bestparty = nil
    for _, party in ipairs(parties) do
        local number = party.votes / party.divisor
        if number > bestnumber then
            bestnumber = number
            bestparty = party
        end
    end
    bestparty.divisor = bestparty.divisor + 1
end

for _, party in ipairs(parties) do
    print(("%3d %s"):format(party.divisor - 1, party.name))
end
