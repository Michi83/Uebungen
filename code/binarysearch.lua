local function binarysearch(needle, haystack)
    local left = 1
    local right = #haystack
    while left <= right do
        local middle = math.floor((left + right) / 2)
        if haystack[middle] > needle then
            right = middle - 1
        elseif haystack[middle] < needle then
            left = middle + 1
        else
            return middle
        end
    end
    return nil
end

local primes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29}
for i = 1, 30 do
    print(i, binarysearch(i, primes))
end
