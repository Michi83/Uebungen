local fertility = 100
local mutationrate = 5
local genepool =
{
    " ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
    "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
}
local target =
{
    "M", "E", "T", "H", "I", "N", "K", "S", " ", "I", "T", " ", "I", "S", " ",
    "L", "I", "K", "E", " ", "A", " ", "W", "E", "A", "S", "E", "L"
}
local parent = {}
for i = 1, #target do
    table.insert(parent, genepool[math.random(#genepool)])
end
print(table.concat(parent))
while true do
    local bestchild
    local bestscore
    for i = 1, fertility do
        local child = {}
        local score = 0
        for j = 1, #target do
            if math.random(100) > mutationrate then
                table.insert(child, parent[j])
            else
                table.insert(child, genepool[math.random(#genepool)])
            end
            if child[j] == target[j] then
                score = score + 1
            end
        end
        if bestscore == nil or score > bestscore then
            bestchild = child
            bestscore = score
        end
    end
    parent = bestchild
    print(table.concat(parent))
    if bestscore == #target then
        break
    end
end
