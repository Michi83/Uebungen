local Position = {}
Position.__index = Position

function Position.new()
    local self = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}
    setmetatable(self, Position)
    self.player = 1
    return self
end

function Position:copy()
    local copy = {{}, {}, {}}
    setmetatable(copy, Position)
    for i = 1, 3 do
        for j = 1, 3 do
            copy[i][j] = self[i][j]
        end
    end
    copy.player = -self.player
    return copy
end

function Position:__tostring()
    local symbols = {}
    for i = 1, 3 do
        for j = 1, 3 do
            if self[i][j] == 1 then
                table.insert(symbols, "X")
            elseif self[i][j] == -1 then
                table.insert(symbols, "O")
            else
                table.insert(symbols, " ")
            end
        end
    end
    local s = "%s | %s | %s\n--+---+--\n%s | %s | %s\n--+---+--\n%s | %s | %s\n"
    s = s:format(unpack(symbols))
    return s
end

function Position:findthreeinarow()
    for i = 1, 3 do
        if self[i][1] ~= 0 then
            if self[i][2] == self[i][1] and self[i][3] == self[i][1] then
                return self[i][1]
            end
        end
        if self[1][i] ~= 0 then
            if self[2][i] == self[1][i] and self[3][i] == self[1][i] then
                return self[1][i]
            end
        end
    end
    if self[2][2] ~= 0 then
        if self[1][1] == self[2][2] and self[3][3] == self[2][2] then
            return self[2][2]
        elseif self[1][3] == self[2][2] and self[3][1] == self[2][2] then
            return self[2][2]
        end
    end
    return 0
end

function Position:generatemoves()
    local moves = {}
    for i = 1, 3 do
        for j = 1, 3 do
            if self[i][j] == 0 then
                local move = self:copy()
                move[i][j] = self.player
                move.notation = tostring(9 - 3 * i + j)
                table.insert(moves, move)
            end
        end
    end
    return moves
end

function Position:maxi()
    local threeinarow = self:findthreeinarow()
    if threeinarow ~= 0 then
        return threeinarow, nil
    end
    local moves = self:generatemoves()
    if #moves == 0 then
        return 0, nil
    end
    local bestscore = -1 / 0
    local bestmove = nil
    for _, move in ipairs(moves) do
        local score, _ = move:mini()
        if score > bestscore then
            bestscore, bestmove = score, move
        end
    end
    return bestscore, bestmove
end

function Position:mini()
    local threeinarow = self:findthreeinarow()
    if threeinarow ~= 0 then
        return threeinarow, nil
    end
    local moves = self:generatemoves()
    if #moves == 0 then
        return 0, nil
    end
    local bestscore = 1 / 0
    local bestmove = nil
    for _, move in ipairs(moves) do
        local score, _ = move:maxi()
        if score < bestscore then
            bestscore, bestmove = score, move
        end
    end
    return bestscore, bestmove
end

local position = Position.new()
print "7 | 8 | 9\n--+---+--\n4 | 5 | 6\n--+---+--\n1 | 2 | 3\n"
while true do
    local moves = position:generatemoves()
    local legal = false
    while not legal do
        local humanmove = io.read()
        for _, move in ipairs(moves) do
            if move.notation == humanmove then
                position = move
                legal = true
                break
            end
        end
    end
    print()
    print(position)
    if position:findthreeinarow() ~= 0 or #position:generatemoves() == 0 then
        break
    end
    _, position = position:mini()
    print(position)
    if position:findthreeinarow() ~= 0 or #position:generatemoves() == 0 then
        break
    end
end
