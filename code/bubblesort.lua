local function bubblesort(array)
    for i = #array, 2, -1 do
        for j = 1, i - 1 do
            if array[j] > array[j + 1] then
                array[j], array[j + 1] = array[j + 1], array[j]
            end
        end
    end
end
