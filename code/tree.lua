local node1 = {value = 1, children = {}}
local node2 = {value = 2, children = {}}
local node3 = {value = 3, children = {}}
local node4 = {value = 4, children = {}}
local node5 = {value = 5, children = {}}
local node6 = {value = 6, children = {}}
local node7 = {value = 7, children = {}}
local node8 = {value = 8, children = {}}
local node9 = {value = 9, children = {}}
local node10 = {value = 10, children = {}}
local node11 = {value = 11, children = {}}
local node12 = {value = 12, children = {}}
local node13 = {value = 13, children = {}}
table.insert(node1.children, node2)
table.insert(node1.children, node3)
table.insert(node1.children, node4)
table.insert(node2.children, node5)
table.insert(node2.children, node6)
table.insert(node2.children, node7)
table.insert(node3.children, node8)
table.insert(node3.children, node9)
table.insert(node3.children, node10)
table.insert(node4.children, node11)
table.insert(node4.children, node12)
table.insert(node4.children, node13)

local function preorder(node)
    print(node.value)
    for _, child in ipairs(node.children) do
        preorder(child)
    end
end

local function postorder(node)
    for _, child in ipairs(node.children) do
        postorder(child)
    end
    print(node.value)
end

local function levelorder(root)
    local queue = {}
    table.insert(queue, root)
    while #queue > 0 do
        local node = table.remove(queue, 1)
        print(node.value)
        for _, child in ipairs(node.children) do
            table.insert(queue, child)
        end
    end
end

print "Pre-Order:"
preorder(node1)
print "Post-Order:"
postorder(node1)
print "Level-Order:"
levelorder(node1)
