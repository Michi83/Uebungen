local random = require "random"

local function shuffle(array)
    for i = #array, 2, -1 do
        local j = random() % i + 1
        array[i], array[j] = array[j], array[i]
    end
end

local array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
shuffle(array)
for _, item in ipairs(array) do
    print(item)
end
