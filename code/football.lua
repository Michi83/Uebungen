local teams = {"A", "B", "C", "D", "E", "F"}

for matchday = 1, #teams - 1 do
    for i = 1, #teams / 2 do
        print(teams[i] .. " - " .. teams[#teams - i + 1])
    end
    print()
    local temp = teams[#teams]
    for i = #teams, 3, -1 do
        teams[i] = teams[i - 1]
    end
    teams[2] = temp
end
