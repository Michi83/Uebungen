# Programmierübungen

Das ist meine kleine Sammlung von Übungen für Programmieranfänger, die ich gelegentlich ergänze. Es handelt sich dabei größtenteils nicht um Übungsaufgaben, die man lösen soll, sondern um Übungsprojekte, die man nachprogrammieren kann. Bei vielen davon geht es um kleine Videospiele.

Die Codebeispiele sind in Lua geschrieben. Man muss aber kein Lua-Experte sein, um die Übungen zu verstehen. Es reicht, wenn man sich oberflächlich ein bisschen Lua aneignet, und dann den Code liest, als wäre er Pseudocode und in der eigenen Lieblingssprache nachprogrammiert. Man muss aber beachten, dass in Lua Arrays ab 1 durchnummeriert werden, statt ab 0 wie in vielen anderen Sprachen.

Das offizielle Buch zu Lua findet sich hier: https://www.lua.org/pil/contents.html

Sehr leicht:
* [Größter gemeinsamer Teiler](text/gcd.md)
* [Kreiszahl π](text/pi.md)
* [Zufallsgenerator](text/random.md)

Leicht:
* [Binäre Suche](text/binarysearch.md)
* [Bubblesort](text/bubblesort.md)
* [Karten mischen](text/shuffle.md)
* [Sieb des Eratosthenes](text/sieve.md)
* [Verhältniswahl](text/election.md)
* [Verknüpfte Listen](text/linkedlist.md)

Mittel:
* [Baumstrukturen](text/tree.md)
* [Dawkins' Wiesel](text/weasel.md)
* [Fußball-Spielplan](text/football.md)
* [Poker](text/poker.md)
* [Quicksort](text/quicksort.md)
* [Stacks und Queues](text/deque.md)

Schwer:
* [Tic-Tac-Toe](text/tictactoe.md)

Sehr schwer:
